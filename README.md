# Desafio
 
## Tecnologias usadas
`Java: 17`
`Spring Boot 3.0.1`
`Apache Maven 3.9.0`

## Como executar
Caso esteja usando uma IDE basta abrir o projeto e rodá-lo<br />
Caso prefira usar o Maven:
```bash
mvn spring-boot:run
```
