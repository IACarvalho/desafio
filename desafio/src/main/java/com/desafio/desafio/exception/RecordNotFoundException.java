package com.desafio.desafio.exception;

public class RecordNotFoundException extends RuntimeException{
  private static final long serialVersionUID = 1L;

  public RecordNotFoundException(Long id) {
    super("Register not found wit id: " + id);
  }
}
