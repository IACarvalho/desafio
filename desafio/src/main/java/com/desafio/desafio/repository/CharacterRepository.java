package com.desafio.desafio.repository;

import com.desafio.desafio.model.Character;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CharacterRepository extends JpaRepository<Character, Long> {
}
