package com.desafio.desafio.repository;

import com.desafio.desafio.model.Location;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LocationRepository extends JpaRepository<Location, Long> {
}
