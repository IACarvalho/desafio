package com.desafio.desafio;

import com.desafio.desafio.model.Location;
import com.desafio.desafio.repository.LocationRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.time.LocalDate;

@SpringBootApplication
public class DesafioApplication {

	public static void main(String[] args) {
		SpringApplication.run(DesafioApplication.class, args);
	}

	@Bean
	CommandLineRunner initDatabse(LocationRepository repository) {
		return args -> {
			repository.deleteAll();

			Location location = new Location();
			location.setName("Teste");
			location.setDimension("teste");
			location.setCreatedAt(LocalDate.now().toString());
			repository.save(location);
		};
	}

}
