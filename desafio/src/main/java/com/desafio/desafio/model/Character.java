package com.desafio.desafio.model;

import java.time.LocalDate;

import com.desafio.desafio.Utils.GenderEnum;
import com.desafio.desafio.Utils.StatusEnum;
import com.fasterxml.jackson.annotation.JsonAlias;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "fanfic_characters")
public class Character {
  
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column
  private String name;

  @Column
  @Enumerated(EnumType.STRING)
  private StatusEnum status;

  @Column
  private String species;

  @Column
  @Enumerated(EnumType.STRING)
  private GenderEnum gender;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumns({
    @JoinColumn(name = "location_name", referencedColumnName="name"),
    @JoinColumn(name = "location_url", referencedColumnName="url")
  })
  private Location location;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumns({
    @JoinColumn(name = "origin_name", referencedColumnName="name"),
    @JoinColumn(name = "origin_url", referencedColumnName="url")
  })
  private Location origin;

  @Column
  private String url;

  @Column(name = "created_at")
  @JsonAlias(value = "created")
  private String createdAt = LocalDate.now().toString();
}
