package com.desafio.desafio.controller;

import com.desafio.desafio.model.Location;
import com.desafio.desafio.service.LocationService;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;

import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Validated
@RestController
@RequestMapping("/locations")
public class LocationController {

  private final LocationService service;

  

  public LocationController(LocationService service) {
    this.service = service;
  }

  @GetMapping
  public List<Location> findAll(){
    return service.findAll();
  }

  @GetMapping("/{id}")
  public Location findById(@PathVariable @NotNull @Positive Long id){
    return service.findById(id);
  }

  @PostMapping
  @ResponseStatus(code = HttpStatus.CREATED)
  public Location create(@RequestBody @Valid Location location) {
    return service.create(location);
  }

  @PutMapping("/{id}")
  public Location update(
    @PathVariable @NotNull @Positive Long id, 
    @RequestBody @Valid Location valuesToUpdate
  ) {
    return service.update(id, valuesToUpdate);
  }

  @DeleteMapping("/{id}")
  @ResponseStatus(code = HttpStatus.NO_CONTENT)
  public void delete(
    @PathVariable @NotNull @Positive Long id
  ){
    service.delete(id);
  }
  
}
