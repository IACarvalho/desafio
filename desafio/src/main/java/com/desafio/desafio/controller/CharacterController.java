package com.desafio.desafio.controller;

import com.desafio.desafio.model.Character;
import com.desafio.desafio.service.CharacterService;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;

import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Validated
@RestController
@RequestMapping("/characters")
public class CharacterController {

  private final CharacterService service;

  public CharacterController(CharacterService service) {
    this.service = service;
  }

  @GetMapping
  public List<Character> findAll() {
    return service.findAll();
  }

  @GetMapping("/{id}")
  public Character findById(
    @PathVariable @NotNull @Positive Long id){
    return service.findById(id);
  }

  @PostMapping
  @ResponseStatus(code = HttpStatus.CREATED)
  public Character create(@RequestBody @Valid Character character) {
    return service.create(character);
  }

  @PutMapping("/{id}")
  public Character update(
    @PathVariable @NotNull @Positive Long id, 
    @RequestBody @Valid Character valuesToUpdate
  ) {
    return service.update(id, valuesToUpdate);
  }

  @DeleteMapping("/{id}")
  @ResponseStatus(code = HttpStatus.NO_CONTENT)
  public void delete(
    @PathVariable @NotNull @Positive Long id
  ){
    service.delete(id);
  }
}
