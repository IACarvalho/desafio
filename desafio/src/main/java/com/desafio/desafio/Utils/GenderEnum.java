package com.desafio.desafio.Utils;

public enum GenderEnum {
  FEMALE,
  MALE,
  GENDERLESS,
  UNKNOWN
}
