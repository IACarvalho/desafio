package com.desafio.desafio.service;

import java.util.List;

import com.desafio.desafio.exception.RecordNotFoundException;
import org.springframework.stereotype.Service;

import com.desafio.desafio.model.Location;
import com.desafio.desafio.repository.LocationRepository;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;

@Service
public class LocationService {
  private final LocationRepository repository;
  private final HttpServletRequest requestUrl;

  public LocationService(LocationRepository repository, HttpServletRequest requestUrl) {
    this.repository = repository;
    this.requestUrl = requestUrl;
  }

  public List<Location> findAll(){
    return repository.findAll();
  }

  public Location findById( @NotNull @Positive Long id){
    return repository.findById(id).orElseThrow(() -> new RecordNotFoundException(id));
  }

  public Location create( @Valid Location location) {
    Location savedLocation =  repository.save(location);
    savedLocation
      .setUrl(requestUrl.getRequestURL().toString() + 
        "/" +savedLocation.getId());
    return repository.save(savedLocation);
  }

  public Location update(
    @NotNull @Positive Long id, 
    @Valid Location valuesToUpdate
  ) {
    return repository.findById(id)
      .map(result -> {
        result.setName(valuesToUpdate.getName());
        result.setDimension(valuesToUpdate.getDimension());
        return repository.save(result);
      }).orElseThrow(() -> new RecordNotFoundException(id));
  }

  public void delete(
    @NotNull @Positive Long id
  ){
    repository.delete(repository.findById(id)
      .orElseThrow(() -> new RecordNotFoundException(id)));
  }
  
}
