package com.desafio.desafio.service;

import java.util.List;

import com.desafio.desafio.exception.RecordNotFoundException;
import com.desafio.desafio.model.Character;

import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import com.desafio.desafio.repository.CharacterRepository;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;

@Validated
@Service
public class CharacterService {
  
  private final CharacterRepository repository;
  private final HttpServletRequest requestUrl;

  public CharacterService(CharacterRepository repository, HttpServletRequest requestUrl) {
    this.repository = repository;
    this.requestUrl = requestUrl;
  }

  public List<Character> findAll() {
    return repository.findAll();
  }

  public Character findById(
    @NotNull @Positive Long id){
    return repository.findById(id).orElseThrow(() -> new RecordNotFoundException(id));
  }

  public Character create(@Valid Character character) {
    Character createdCharacter = repository.save(character);
    createdCharacter.setUrl(requestUrl.getRequestURL() + 
      "/" + createdCharacter.getId());
    return repository.save(createdCharacter);
  }

  public Character update(
    @NotNull @Positive Long id, 
    @Valid Character valuesToUpdate
  ) {
    return repository.findById(id)
      .map(result -> {
        result.setName(valuesToUpdate.getName());
        result.setGender(valuesToUpdate.getGender());
        result.setSpecies(valuesToUpdate.getSpecies());
        result.setStatus(valuesToUpdate.getStatus());
        
        return repository.save(result);
      }).orElseThrow(() -> new RecordNotFoundException(id));
  }

  public void delete(
    @NotNull @Positive Long id
  ){
    repository.delete(repository.findById(id)
      .orElseThrow(() -> new RecordNotFoundException(id)));
  }

}
